<?php namespace Voop\Xunit;

/**
 * постащик классов для самотеста
 *
 * Class XunitCoreTestMapProvider
 *
 * @package Voop\Xunit
 */
class XunitCoreTestMapProvider
{
    /**
     * @return array
     */
    public function map()
    {
        return [
            \Voop\Test\XunitStateTest::class,
            \Voop\Test\XunitAssertsTest::class,
            \Voop\Test\XunitCounterTest::class,
        ];
    }
}
