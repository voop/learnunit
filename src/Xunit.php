<?php namespace Voop\Xunit;


/**
 * Class Xunit
 *
 * @package Voop\Xunit
 */
class Xunit
{
    /**
     * @var bool
     */
    public $wasRun = false;

    /**
     * @var bool
     */
    public $setUp = false;

    /**
     * @var array
     */
    public static $errLog = [];

    /** @var - состояние мастер-объекта xUnit */
    private static $xunitState;


    /**
     * Сетчик полного количества тестов
     */
    private $counter = 0;


    /**
     */
    public function __construct()
    {
    }

    /**
     *
     */
    public function setUp()
    {
        $this->setUp = true;
    }

    /**
     *
     */
    public function run()
    {
        // привязывем состояние мастер-объекта к статику
        self::$xunitState = $this;

        // кто породил объект
        $class = static::class;

        // ищем тест-методы
        $reflexionClass = new \ReflectionClass($class);
        $methods = $reflexionClass->getMethods();

        // запустить кастомный init-метод, запустится ПЕРЕД всей инициализацией
        $this->_process($class, 'start');

        // ------------ go -----------------------
        // подтвердить запуск кейсов
        $this->setUp();

        // запускаем только тест-методы, а остальное отфильтровываем
        foreach ($methods as $methodInfo) {

            // только методы которые начинаются с "test" или стартовый метод
            if (false === strpos($methodInfo->name, 'test')) {
                continue;
            }
            // счетчик запуска
            $this->counter++;
            $this->_process($class, $methodInfo->name);
        }

        $this->wasRun = true;
        // ------------ end -----------------------

        // кастомный drop-метод after - запустится В САМОМ
        $this->_process($class, 'end');
    }


    /**
     * @param string $class
     * @param string $methodName
     */
    private function _process(string $class, string $methodName)
    {
        try {
            // на каждый тест-метод свой объект
            $obj = new $class();
            $obj->$methodName();

            // TODO - вызов метода-чистильщика $obj->cleanUp()

            echo $this->visualize(true);
        } catch (\Exception $e) {
            self::$errLog[] = sprintf("Error: %s::%s(); %s", $class, $methodName, $e->getMessage());

            // TODO - тест правильного попадания ошибок в лог
            // TODO - вызов метода-чистильщика $obj->cleanUp()

            echo $this->visualize(false);
        }
    }


    /**
     *
     */
    public function start()
    {
    }


    /**
     *
     */
    public function end()
    {
    }


    /**
     * @return array
     */
    public static function getLog()
    {
        return self::$errLog;
    }


    /**
     * @return int
     */
    public function getCounter()
    {
        return $this->counter;
    }


    /**
     * @return \Voop\Xunit\Xunit
     */
    public function getMasterState()
    {
        return self::$xunitState;
    }

    /**
     * @param bool $val
     * @return string
     */
    protected function visualize(bool $val)
    {
        return $val ? '.' : 'E';
    }


    /**
     * @param mixed $val
     * @param null  $comment
     * @return bool
     * @throws \Exception
     */
    public function assertTrue($val, $comment = null)
    {
        if ($val !== true) {
            $value = $this->_stringifyVal($val);
            $comment = $comment ? '; "' . $comment . '"' : '';
            throw new \Exception(sprintf('Assert fail: %s is not true %s', $value, $comment));
        }

        return true;
    }


    /**
     * @param mixed $val
     * @param null  $comment
     * @return bool
     * @throws \Exception
     */
    public function assertFalse($val, $comment = null)
    {
        if ($val !== false) {
            $value = $this->_stringifyVal($val);
            $comment = $comment ? '; "' . $comment . '"' : '';
            throw new \Exception(sprintf('Assert fail: %s is not false %s', $value, $comment));
        }

        return true;
    }

    /**
     * @param mixed $value1
     * @param mixed $value2
     * @param null  $comment
     * @return bool
     * @throws \Exception
     */
    public function assertEquals($value1, $value2, $comment = null)
    {
        if ($value1 != $value2 || gettype($value1) != gettype($value2)) {
            $value1 = $this->_stringifyVal($value1);
            $value2 = $this->_stringifyVal($value2);
            $comment = $comment ? '; "' . $comment . '"' : '';
            throw new \Exception(sprintf('Assert fail: %s is not equals %s %s', $value1, $value2, $comment));
        }

        return true;
    }

    /**
     * @param mixed $value
     * @return mixed|string
     */
    private function _stringifyVal($value)
    {
        $value = print_r($value, true);
        return is_null($value) ? 'NULL' : $value;
    }
}
