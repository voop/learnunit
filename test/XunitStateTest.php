<?php namespace Voop\Test;

use Voop\Xunit\Xunit;

/**
 * Self-тест методов
 *
 * Class XunitStateTest
 *
 * @package Voop\Test
 */
class XunitStateTest extends Xunit
{
    /**
     * Запустим "Стартовый метод"
     * Проверить, что при запуске Стартового метода
     * НИЧЕГО не запущено и еще НЕ проинициализировано
     */
    public function start()
    {
        $this->assertFalse($this->getMasterState()->wasRun);
        $this->assertFalse($this->getMasterState()->wasRun);
    }

    /**
     *
     */
    public function testSetUp()
    {
        $this->assertTrue($this->getMasterState()->setUp);
    }

    /**
     * Запустим "Финишный метод".
     * Проверить, что при запуске Финишного метода
     * ВСЁ правильно проинициализировано
     */
    public function end()
    {
        $this->assertTrue($this->getMasterState()->wasRun);
    }
}
