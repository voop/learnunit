<?php namespace Voop\Test;

use Voop\Xunit\Xunit;

/**
 * Self-Тест
 *  - Счетчика запуска количества протестированных в текущем кейсе методов
 *  - Проверки количества доступныйх для теста методов в тест-классе
 */
class XunitCounterTest extends Xunit
{
    /**
     * Запустить первый тестовый метод в кейсе
     */
    public function testMethod1()
    {
        // Проверить что метод учтен и всего пока запущен 1 метод в кейсе
        $this->assertEquals(1, $this->getMasterState()->getCounter());
    }

    /**
     * Запустить второй тестовый метод в кейсе
     */
    public function testMethod2()
    {
        // Проверить что метод тоже учтен и всего пока запущено 2 метода в кейсе
        $this->assertEquals(2, $this->getMasterState()->getCounter());
    }

    /**
     * Это НЕ тестовый метод - не начинается с "test" - НЕ должен запускаться
     *
     * @throws \Exception
     */
    public function someNotTestMethod()
    {
        throw new \Exception('Alert! Not test method was run!!!');
    }

    /**
     * Третий тестовый метод
     */
    public function testMethod3()
    {
        // Проверить что метод тоже учтен и всего пока запущено 3 метода в кейсе
        $this->assertEquals(3, $this->getMasterState()->getCounter());
    }
}
