<?php namespace Voop\Test;

use Voop\Xunit\Xunit;

/**
 * Проверка Assert`ов
 *
 * Class XunitAssertsTest
 *
 * @package Voop\Test
 */
class XunitAssertsTest extends Xunit
{
    /**
     * Тест "Проверки на Эквивалент = True"
     */
    public function testAssertTrue()
    {
        $try = [
            [true,  true,  "true === true"],
            [false, false, "false !== true"],
            [1,     false, "1 !== true"],
            ['1',   false, "'1' !== true"],
            [null,  false, "null !== true"],
            [[],    false, "[] !== true"],
        ];

        foreach ($try as list($val, $expected, $comment)) {
            $xunut = $this;
            $this->_assertsTest(function () use ($xunut, $val, $comment) {
                $this->assertTrue($val, $comment);
            }, $expected, $comment, __METHOD__);
        }
    }


    /**
     * Тест "Проверки на Эквивалент = False"
     */
    public function testAssertFalse()
    {
        $try = [
            [false, true,  "false === false"],
            [true,  false, "true !== false"],
            [1,     false, "1 !== false"],
            ['1',   false, "'1' !== false"],
            ['0',   false, "'0' !== false"],
            [0,     false, "0 !== false"],
            [null,  false, "null !== false"],
            [[],    false, "[] !== false"],
        ];

        foreach ($try as list($val, $expected, $comment)) {
            $xunut = $this;
            $this->_assertsTest(function () use ($xunut, $val, $comment) {
                $this->assertFalse($val, $comment);
            }, $expected, $comment, __METHOD__);
        }
    }


    /**
     * Тест "Проверки на Эквивалент = False"
     */
    public function testAssertEquals()
    {
        $try = [
            [[0, 0],        true,  "0 == 0"],
            [[1, 1],        true,  "1 == 1"],
            [[true, true],  true,  "true == true"],
            [['a', 'a'],    true,  "'a' == 'a'"],
            [[[0], [0]],    true,  "[0] == [0]"],
            [[[], []],      true,  "[] == []"],
            [[[], 1],       false, "[] != 1"],
            [[false, 1],    false, "false != 1"],
            [[1, false],    false, "1 != false"],
            [[true, false], false, "true != false"],
            [[[], 0],       false, "[] != 0"], // ?? что к чему преобразовывается??
            [['a', 1],      false, "'a' != 1"],
            [['a', 'b'],    false, "'a' != 'b'"],
            [[2, false],    false, "2 != false"],
            [[true, 1],     false, "true != 1"],
            [['1', 1],      false, "'1' != 1"],
            [['', 0],       false, "'' != 0"],
            [['0', 0],      false, "'0' != 0"],
        ];

        foreach ($try as list($val, $expected, $comment)) {
            $xunut = $this;
            $this->_assertsTest(function () use ($xunut, $val, $comment) {
                $xunut->assertEquals($val[0], $val[1], $comment);
            }, $expected, $comment, __METHOD__);
        }
    }


    /**
     * @param callable $call
     * @param mixed    $expected
     * @param mixed    $comment
     * @param mixed    $method
     * @return bool
     * @throws \Exception
     */
    private function _assertsTest(callable $call, $expected, $comment, $method)
    {
        // прехватывам Exception ДО того как его перехватит обработчик Xunit::run()
        try {
            $call();

            // ожидалось что всё пройдёт успешно
            if (true === $expected) {
                return true;
            }

            $message = "Error with: $comment";

        } catch (\Exception $e) {

            // Если вывалится Ecxception - проверяем что сознательно подпихнули левое значение и ожидали именно Exception
            if (false === $expected) {
                return true;
            }

            $message = $e->getMessage();
        }

        // что-то пошло не так ...
        throw new \Exception(sprintf('Fail test %s(); "%s"', $method, $message));
    }
}
